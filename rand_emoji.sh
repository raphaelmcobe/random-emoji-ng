#!/usr/bin/env bash

emoji=$(head -1 emojis.txt)
echo -e "\U${emoji}"
